
{
    'name': 'KOKO CRM Extension',
    'version': '1.1',
    'author': 'Prolitus Technologies Pvt. Ltd',
    'website': 'www.prolitus.com',
    'category': 'CRM',
    'depends': ['crm'],
    'demo': [

    ],
    'description': """
    CRM Extension.
========================================================================
    """,
    'data': [
        'security/security_view.xml',
        'security/ir.model.access.csv',
        'views/crm_view.xml',
        'views/inspection_configuration_view.xml',
        'views/inspection_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}

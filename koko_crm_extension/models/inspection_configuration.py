# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class InspectionSection(models.Model):
    _name = "inspection.section"
    _description = 'Inspection Section'
    _inherit = ['mail.thread']
    _rec_name = 'name'
    order = 'sequence, id'

    name = fields.Char("Name", required=True)
    sequence = fields.Integer(help="Determine the display order", default=10)
    inspection_tasks_conf_ids = fields.Many2many("inspection.tasks.conf", string='Inspection Task')


class InspectionTasksConf(models.Model):
    _name = "inspection.tasks.conf"
    _description = 'Inspection Tasks'
    _inherit = ['mail.thread']
    _rec_name = 'name'

    inspection_selection = [('char', 'Text'), ('float', 'Real'), ('integer', 'Integer'), ('selection', 'Single Selection'), ('many2many', 'Multi Selection'), ('checkbox', 'Checkbox'), ('text', 'Multiline Text')]

    name = fields.Char("Task", required=True)
    inspection_type = fields.Selection(inspection_selection, string='Type', required=True)
    value_ids = fields.One2many("inspection.tasks.value", 'inspection_conf_id', string='Inspection Value')
    is_image = fields.Boolean('Image')
    task_info = fields.Text("Information")


class InspectionTasksValue(models.Model):
    _name = "inspection.tasks.value"
    _description = 'Inspection Tasks Value'
    _inherit = ['mail.thread']
    _rec_name = 'name'

    name = fields.Char("Name", required=True)
    inspection_conf_id = fields.Many2one('inspection.tasks.conf', string='Inspection Configuration')


class RescheduleReasonConf(models.Model):
    _name = "reschedule.reason.conf"
    _description = 'Reschedule reason'
    _inherit = ['mail.thread']
    _rec_name = 'name'

    name = fields.Char("Name", required=True)
    is_comment = fields.Boolean('Is Comment')


class CrmStageInherit(models.Model):
    _inherit = "crm.stage"

    create_inspection_from_this_stage = fields.Boolean("Generate Inspection")

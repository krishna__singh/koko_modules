# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
import datetime
from odoo import SUPERUSER_ID


class Inspection(models.Model):
    _name = "inspection"
    _description = 'Inspection'
    _inherit = ['mail.thread']
    _rec_name = 'ref'

    @api.multi
    def _compute_total_pass_score(self):
        for rec in self:
            total_pass = 0
            total_pass_fail = 0
            for modification in rec.modification_ids:
                total_pass += modification.total_score
                total_pass_fail += modification.total_pass_fail
            rec.total_score = str(total_pass) + ' / ' + str(total_pass_fail)

    agent_selection = [('agent_with_location', 'Agent With Location'), ('agent_without_location', 'Agent Without Location'), ('nbwl', 'Network Builder With Location'), ('nbwl', 'Network Builder Without Location'), ('host', 'Host'), ('other', 'Other')]

    ref = fields.Char(readonly=True, string='Inspection')
    old_ref = fields.Char(readonly=True, string='Ref')
    user_id = fields.Many2one('res.users', string='Inspector', domain=lambda self: [("groups_id", "=", self.env.ref("koko_crm_extension.group_inspection_user").id)])
    date_of_inspection = fields.Datetime('Date Of Inspection')
    lead_id = fields.Many2one('crm.lead', string='Opportunity')
    agent_name = fields.Char(related='lead_id.contact_name', stirng='Agent Name')
    shop_name = fields.Char(related='lead_id.shop_name', stirng='Shop Name')
    manager_name = fields.Char(related='lead_id.shop_manager_name', stirng='Shop Manager Name')
    manager_contact = fields.Char(related='lead_id.manager_phone_number', stirng='Shop Manager Contact')
    street = fields.Char(related='lead_id.street')
    street2 = fields.Char(related='lead_id.street2')
    zip = fields.Char(related='lead_id.zip')
    city = fields.Char(related='lead_id.city')
    state_id = fields.Many2one(related='lead_id.state_id', string='State', ondelete='restrict')
    country_id = fields.Many2one(related='lead_id.country_id', string='Country', ondelete='restrict')
#     outlet_type = fields.Selection(selection=[('supermarket', 'Supermarket'), ('shop', 'Shop'), ('other', 'Other')], string='Outlet Type')
#     outlet_size = fields.Selection(selection=[('large', 'Large'), ('medium', 'Medium'), ('small', 'Small')], string='Outlet Size')
    pos_system = fields.Selection(related='lead_id.pos_system', string='POS system?')
    lattitude = fields.Char(related='lead_id.lattitude', string='Latitude')
    longitude = fields.Char(related='lead_id.longitude', string='Longitude')
    summary_of_inspection = fields.Text(string='Summary of Inspection')
    total_score = fields.Char('Total Score', compute="_compute_total_pass_score")
    total_estimation_cost = fields.Float('Total Estimation Cost')
    state = fields.Selection([('draft', 'Draft'), ('scheduled', 'Scheduled'), ('rescheduled', 'Rescheduled'),
                              ('done', 'Inspection Done'), ('review', 'Reviewed'), ('cancel', 'Cancelled')], string='State', readonly=True, default='draft',)
    inspection_task_ids = fields.One2many('inspection.task', 'inspection_id', string='Inspection Task')
    reschedule_details_ids = fields.One2many('reschedule.details', 'inspection_id', string='Reschedule Details')
    inspection_suggestion_ids = fields.One2many('inspection.suggestion', 'inspection_id', string='Inspection Suggestions')
    modification_ids = fields.One2many('modification.report', 'inspection_id', string='Inspection Suggestions')
    old_ref_flag = fields.Boolean("Ref Flag")
    done_by_app = fields.Boolean("Done by Mobile App", copy=False)
    max_image = fields.Integer("Max Images", default=38,)
    tdr_rm_id = fields.Many2one(related='lead_id.tdr_rm_id', string='TDR/RM')
    nearest_landmark = fields.Char(related='lead_id.nearest_landmark', string='Nearest Landmarks')
    own_recor_user_id = fields.Many2one('res.users', string='Own Record User')
    agent_contact = fields.Char(related='lead_id.mobile', string='Agent Contact')
    location_accuracy = fields.Char(related='lead_id.location_accuracy', string='Location Accuracy (In Meters)')

    @api.one
    def schedule_inspection(self):
        self.state = 'scheduled'

    @api.multi
    def reset_inspection(self):
        self.state = 'draft'

    @api.multi
    def reschedule_inspection(self):
        return {
            "type": "ir.actions.act_window",
            "name": "Inspection",
            "target": "new",
            "res_model": "reschedule.inspection.wizard",
            "view_mode": "form",
            "view_type": "form",
            "context": {"default_user_id": self.user_id.id}
        }

    @api.one
    def review_inspection(self):
        self.state = 'review'

    @api.one
    def reset_review_inspection(self):
        self.state = 'done'

#     @api.multi
#     def inprogress_inspection(self):
#         self.state = 'in-progress'

    @api.multi
    def done_inspection(self):
        #self.state = 'done'
        return {
            "type": "ir.actions.act_window",
            "name": "Inspection",
            "target": "new",
            "res_model": "inspection.done.wizard",
            "view_mode": "form",
            "view_type": "form",
            "context": {"default_ref": self.ref}
        }

    @api.multi
    def cancel_inspection(self):
        self.state = 'cancel'

    @api.onchange('country_id')
    def _onchange_country_id(self):
        if self.country_id:
            return {'domain': {'state_id': [('country_id', '=', self.country_id.id)]}}
        else:
            return {'domain': {'state_id': []}}

    @api.onchange('user_id')
    def _onchange_user_id(self):
        if self.user_id:
            superuser = self.env['res.users'].search([('id', '=', SUPERUSER_ID)])
            inspector_user = self.env['res.users'].search([("groups_id", "=", self.env.ref("koko_crm_extension.group_inspection_user").id)])
            if self.env.user == superuser:
                return
            else:
                if self.env.user != self.user_id and self.env.user in inspector_user:
                    raise UserError(_('Inspector cannot select another Inspector in Inspection.'))

    @api.model
    def create(self, vals):
        inspection_task_list = self.env['inspection.task']
        modification_list = self.env['modification.report']
        inspection_section_conf_list = self.env['inspection.section'].search([])
        vals['ref'] = self.env['ir.sequence'].next_by_code('inspection')
        vals['own_recor_user_id'] = self.env.user.id
        res = super(Inspection, self).create(vals)
        inspection_list = inspection_task_list.search([('inspection_id', '=', res.id)])
        modification = modification_list.search([('inspection_id', '=', res.id)])
        if not inspection_list:
            for section in inspection_section_conf_list:
                for task_conf in section.inspection_tasks_conf_ids:
                    inspection_task_list.create({
                        'section_id': section.id,
                        'name': task_conf.name,
                        'inspection_type': task_conf.inspection_type,
        #                     'is_image': task_conf.is_image,
                        'inspection_id': res.id,
                        'inspection_config_id': task_conf.id})
        if not modification:
            for section in inspection_section_conf_list:
                modification_list.create({
                    'section_id': section.id,
                    'inspection_id': res.id})
        return res


class ModificationReport(models.Model):
    _name = "modification.report"

    @api.multi
    def _compute_total_pass_score(self):
        for rec in self:
            if rec.inspection_id:
                pass_fail_count = 0
                count_pass = 0
                for task in rec.inspection_id.inspection_task_ids:
                    if task.section_id == rec.section_id:
                        if task.inspection_type == 'checkbox':
                            if task.checkbox_value:
                                count_pass += 1
                            pass_fail_count += 1
                        final_pass_score = str(count_pass) + ' / ' + str(pass_fail_count)
                rec.total_pass_fail = pass_fail_count
                rec.total_score = count_pass
                rec.total_pass_score = final_pass_score or ''

    section_id = fields.Many2one('inspection.section', 'Section')
    total_pass_score = fields.Char('Total Score', compute="_compute_total_pass_score")
    total_score = fields.Integer('Total Score', compute="_compute_total_pass_score")
    total_pass_fail = fields.Integer('Total Pass Fail', compute="_compute_total_pass_score")
    total_estimation_cost = fields.Float('Total Estimation Cost')
    modification_suggestion_id = fields.Many2one('modification.suggestions', 'Modification Suggestion')
    inspection_suggestion_ids = fields.One2many('inspection.suggestion', 'modification_report_id', string='Inspection Suggestions')
    inspection_id = fields.Many2one('inspection', string='Inspection')


class InspectionSuggestion(models.Model):
    _name = "inspection.suggestion"

    modification_suggestion_id = fields.Many2one('modification.suggestions', 'Modification Suggestion')
    partner_ids = fields.Many2many('res.partner', string=' ')
    other = fields.Boolean('Other Suggestion')
    suggestion = fields.Char('Modification Suggestion')
    value = fields.Char('Modification Suggestion')
    partner_value = fields.Char('Preferred Vendors')
    inspection_id = fields.Many2one('inspection', string='Inspection')
    modification_report_id = fields.Many2one('modification.report', string='Modification Report')

    @api.depends('modification_suggestion_id')
    @api.onchange('modification_suggestion_id')
    def onchange_modification_suggestion_id(self):
        value = ''
        if self.modification_suggestion_id:
            value = str(self.modification_suggestion_id.name)
        self.value = value

    @api.depends('other')
    @api.onchange('other')
    def onchange_other(self):
        if self.other:
            self.modification_suggestion_id = False
            self.suggestion = False

    @api.depends('suggestion')
    @api.onchange('suggestion')
    def onchange_suggestion(self):
        suggestion = ''
        if self.suggestion:
            suggestion = str(self.suggestion)
        self.value = suggestion

    @api.depends('partner_ids')
    @api.onchange('partner_ids')
    def onchange_partner_ids(self):
        final_text = ''
        for value in self.partner_ids:
            final_text = final_text + str(value.name) + ','
        self.partner_value = final_text


class InspectionTask(models.Model):
    _name = "inspection.task"

    inspection_selection = [('char', 'Text'), ('float', 'Real'), ('integer', 'Integer'), ('selection', 'Single Selection'), ('many2many', 'Multi Selection'), ('checkbox', 'Checkbox'), ('text', 'Multiline Text')]
    section_id = fields.Many2one('inspection.section', 'Section',)
    name = fields.Char('Task Name', required=True)
    inspection_type = fields.Selection(inspection_selection, string='Type of Tasks')
    char_vale = fields.Char('Value')
    float_value = fields.Float('Value')
    integer_value = fields.Integer('Value')
    selection_id = fields.Many2one('inspection.tasks.value', string='Value')
    many2many_value = fields.Many2many('inspection.tasks.value', string=' ')
    checkbox_value = fields.Boolean('Value')
    text_value = fields.Text('Value')
    inspection_id = fields.Many2one('inspection', string='Inspection')
    value = fields.Char('Value')
#     is_image = fields.Boolean('Is Image')
    inspection_config_id = fields.Many2one('inspection.tasks.conf', string='Inspection Configuration')
    image_ids = fields.Many2many('ir.attachment', 'img_inspection_task_ir_attachments_rel', 'image_inspection_task_id', 'image_task_id', string="Images")
    comments = fields.Text("Comments")

    @api.depends('float_value')
    @api.onchange('float_value')
    def onchange_float_value(self):
        float_value = ''
        if self.float_value:
            float_value = str(self.float_value)
        self.value = float_value

    @api.depends('integer_value')
    @api.onchange('integer_value')
    def onchange_integer_value(self):
        integer_value = ''
        if self.integer_value:
            integer_value = str(self.integer_value)
        self.value = integer_value

    @api.depends('selection_id')
    @api.onchange('selection_id')
    def onchange_selection_id(self):
        selection_id = ''
        if self.selection_id:
            selection_id = self.selection_id.name
        self.value = selection_id

    @api.depends('char_vale')
    @api.onchange('char_vale')
    def onchange_char_vale(self):
        char_vale = ''
        if self.char_vale:
            char_vale = str(self.char_vale)
        self.value = char_vale

    @api.depends('many2many_value')
    @api.onchange('many2many_value')
    def onchange_many2many_value(self):
        final_text = ''
        for value in self.many2many_value:
            final_text = final_text + str(value.name) + ','
        self.value = final_text

    @api.depends('checkbox_value')
    @api.onchange('checkbox_value')
    def onchange_checkbox_value(self):
        value = 'Fail'
        if self.checkbox_value:
            value = 'Pass'
        self.value = value

    @api.depends('text_value')
    @api.onchange('text_value')
    def onchange_text_value(self):
        final_text = ''
        if self.text_value:
            final_text = self.text_value
        self.value = final_text


class RescheduleDetails(models.Model):
    _name = "reschedule.details"
    _description = 'Reschedule Details'
    _inherit = ['mail.thread']
    _rec_name = 'user_id'

    rescheduled_date = fields.Datetime('Reschedule Date')
    user_id = fields.Many2one('res.users', 'Reschedule By')
    initial_inspection_date = fields.Datetime('Initial Inspection Date')
    new_inspection_date = fields.Datetime('New Inspection Date')
    reason_of_reschedule = fields.Char('Other Reason')
    inspection_id = fields.Many2one('inspection', string='Inspection')
    reschedule_reason_id = fields.Many2one('reschedule.reason.conf', string='Reschedule Reason')


class ModificationSuggestions(models.Model):
    _name = "modification.suggestions"
    _description = 'Modification Suggestions'
    _inherit = ['mail.thread']
    _rec_name = 'name'

    name = fields.Char('Name', required=True)


class RescheduleInspectionWizard(models.TransientModel):
    _name = "reschedule.inspection.wizard"

    reason_name = fields.Char("Reason")
    user_id = fields.Many2one('res.users', string='Inspector')
    reschedule_reason_id = fields.Many2one('reschedule.reason.conf', string='Reschedule Reason')
    is_comment = fields.Boolean(related="reschedule_reason_id.is_comment", string='Is Comment')
    date_of_inspection = fields.Datetime('Date Of Inspection', required="1")

    def create_reshedule_inspection(self):
        current_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        if current_time > str(self.date_of_inspection):
            raise UserError(_('Date and time of rescheduling must be greater then current date time.'))
        else:
            reschedule_inspection = self.env['reschedule.details']
            inspection = self.env['inspection'].browse(self._context.get('active_id'))
            initial_inspection_date = inspection.date_of_inspection
            inspection.write({
                'state': 'rescheduled',
                'date_of_inspection': self.date_of_inspection})
            reschedule_inspection.create({
                'rescheduled_date': datetime.datetime.now(),
                'user_id': self.user_id.id,
                'inspection_id': inspection.id,
                'new_inspection_date': self.date_of_inspection,
                'initial_inspection_date': initial_inspection_date,
                'reschedule_reason_id': self.reschedule_reason_id.id,
                'reason_of_reschedule': self.reason_name})


class InspectionDoneWizard(models.TransientModel):
    _name = "inspection.done.wizard"

    ref = fields.Char("Ref")
    yes = fields.Boolean(string='Yes')
    no = fields.Boolean(string='No', default=True)
    question = fields.Text(string='Reason', default="Do you want create again inspection for this agent ?")
    date_of_inspection = fields.Datetime('Expected Date Of Inspection')

    @api.depends('yes')
    @api.onchange('yes')
    def onchange_yes(self):
        if self.yes:
            self.no = False

    @api.depends('no')
    @api.onchange('no')
    def onchange_no(self):
        if self.no:
            self.yes = False

    def create_inspection(self):
        inspection = self.env['inspection'].browse(self._context.get('active_id'))
        if self.no:
            inspection.write({
                'state': 'done'})
            return
        inspection.write({
            'state': 'done'})
        copy_id = inspection.copy(default=None)
        copy_id.write({'old_ref_flag': True,
                       'old_ref': inspection.ref,
                       'date_of_inspection': self.date_of_inspection})

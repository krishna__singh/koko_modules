# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from docutils.nodes import field


class CrmLeadInherit(models.Model):
    _inherit = "crm.lead"

    @api.multi
    def _get_inspection(self):
        for inspection in self:
            inspection_ids = self.env['inspection'].search([('lead_id', '=', inspection.id)])
            inspection.update({
                'crm_inspection_count': len(set(inspection_ids.ids)),
            })

    phone_number2 = fields.Char('Phone Number 2')
    nearest_landmark = fields.Char('Nearest Landmarks')
    year_in_business = fields.Integer('Years in Business')
    number_of_employee = fields.Integer('Number of Employees')
    lattitude = fields.Char('GPS Location')
    longitude = fields.Char('Longitude')
    pos_system = fields.Selection(selection=[('yes', 'Yes'), ('no', 'No')], string='POS System')
    shop_manager_name = fields.Char('Shop Manager Name')
    manager_phone_number = fields.Char('Shop Manager Phone Number')
    number_of_kps = fields.Integer('Numbers of KPs')
    inspection_stage = fields.Boolean(related='stage_id.create_inspection_from_this_stage', string='Create Inspection for this stage')
    inspection_boolean = fields.Boolean('Inspection Flag')
    crm_inspection_count = fields.Integer(string='# Of Inspection', compute='_get_inspection', readonly=True)
    contract_file = fields.Binary('Upload Contract')
    contract_file_name = fields.Char('File Name')
    contract_selection = fields.Selection(selection=[('signed', 'Signed')], string='Contract', readonly=True)
    shop_name = fields.Char(stirng='Shop Name')
    tdr_rm_id = fields.Many2one('res.users', string='TDR/RM')
    location_accuracy = fields.Char('Location Accuracy (In Meter)')
    neighborhood = fields.Char('neighborhood')

    @api.onchange('user_id')
    def onchange_user_id(self):
        if self.user_id:
            self.tdr_rm_id = self.user_id.id

    @api.multi
    def opne_inspection_wizard(self):
        return {
            "type": "ir.actions.act_window",
            "name": "Inspection",
            "target": "new",
            "res_model": "inspection.wizard",
            "view_mode": "form",
            "view_type": "form",
        }

    @api.multi
    def action_inspection_view(self):
        mod_obj = self.env['ir.model.data']
        inspection_ids = self.env['inspection'].search([('lead_id', '=', self.id)])
        dummy, action_id = tuple(mod_obj.get_object_reference('koko_crm_extension', 'action_inspection'))
        [action] = self.env['ir.actions.act_window'].browse(action_id).read()
        if len(inspection_ids) > 0:
            action['domain'] = [('id', 'in', inspection_ids.ids)]
        elif len(inspection_ids) == 1:
            action['views'] = [(self.env.ref('koko_crm_extension.inspection_view_form').id, 'form')]
            action['res_id'] = inspection_ids.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.model
    def create(self, vals):
        if 'contract_file' in vals and vals.get('contract_file'):
            vals['contract_selection'] = 'signed'
        res = super(CrmLeadInherit, self).create(vals)
        return res

    @api.multi
    def write(self, vals):
        if 'contract_file' in vals and not vals['contract_file']:
            vals['contract_selection'] = False
        if 'contract_file' in vals and vals['contract_file']:
            vals['contract_selection'] = 'signed'
        res = super(CrmLeadInherit, self).write(vals)
        return res


class InspectionWizard(models.TransientModel):
    _name = "inspection.wizard"

    user_id = fields.Many2one('res.users', string='Inspector', required="1", domain=lambda self: [("groups_id", "=", self.env.ref("koko_crm_extension.group_inspection_user").id)])
    date_of_inspection = fields.Datetime('Date Of Inspection', required="1")

    def create_inspection(self):
        inspection = self.env['inspection']
        crm = self.env['crm.lead'].browse(self._context.get('active_id'))
        crm.write({
            'inspection_boolean': True})
        inspection.create({
            'user_id': self.user_id.id,
            'date_of_inspection': self.date_of_inspection,
            'lead_id': crm.id,
            'state': 'scheduled'})


class ResPartnerInherit(models.Model):
    _inherit = "res.partner"

    agent_selection = [('agent_with_location', 'Agent With Location'), ('agent_without_location', 'Agent Without Location'), ('nbwl', 'Network Builder With Location'), ('nbwl', 'Network Builder Without Location'), ('host', 'Host'), ('other', 'Other')]
    
    mobile_sync = fields.Boolean(string='Mobile Sync')
    is_agent = fields.Boolean(string='Is a Agent')
    agent_type = fields.Selection(agent_selection, string='Agent Type')
    other = fields.Char('Other')

# -*- encoding: utf-8 -*-
# Part of Prolitus Technologies Pvt. Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': 'KOKO CRM Advance Invoice',
    'version': '1.0',
    'category': 'CRM',
    'author': 'Prolitus Technologies Pvt. Ltd.',
    'sequence': 50,
    'summary': 'CRM Advance Invoice',
    'depends': ['base', 'crm', 'account'],
    'website': 'www.prolitus.com',
    'description': """
         This module helps to create advance invoice from CRM.
    """,
    'data': [
            'security/ir.model.access.csv',
            'views/account_invoice_inherit_view.xml',
            'views/crm_stage_inherit_view.xml',
            'views/crm_inherit_view.xml',
                ],
    'qweb': [],
    'application': True,
    'installable': True,
    'auto_install': False,
}

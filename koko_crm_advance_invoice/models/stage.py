
from odoo import api, fields, models


class Stage(models.Model):
    _inherit = 'crm.stage'

    invoice_stage = fields.Boolean('Generate Advance Invoice')

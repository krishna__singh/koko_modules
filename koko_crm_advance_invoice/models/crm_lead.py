from odoo import api, fields, models, _
import datetime
from odoo.exceptions import UserError, ValidationError


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    @api.multi
    def _get_invoiced(self):
        for order in self:
            invoice_ids = self.env['account.invoice'].search([('crm_lead_id', '=', order.id)])
            order.update({
                'crm_invoice_count': len(set(invoice_ids.ids)),
                'invoice_ids': invoice_ids.ids,
            })

    @api.multi
    def _get_amount_paid(self):
        for order in self:
            invoice_list = self.env['account.invoice'].search([('id', '=', self.invoice_id.id)])
            if invoice_list.state == 'paid':
                paid_amount = (invoice_list.amount_total - invoice_list.residual)
                order.update({
                    'amount_paid': paid_amount,
                    'amount_pending': invoice_list.residual
                })

    invoice_stage = fields.Boolean(related='stage_id.invoice_stage', string='Create invoice for this stage')
    down_payment_amount = fields.Float(string='Down payment Amount')
    amount_due_before_scheduled_install = fields.Float(string='Amount Due before Scheduled Install')
    loan_amount = fields.Float(string='Loan Amount')
    interest_rate = fields.Float(string='Interest Rate')
    commission_per_liter_fuel_sold = fields.Float(string='Commission per Liter of Fuel Sold')
    stove_facilitation_fee = fields.Float(string='Stove Facilitation Fee')
    stove_sales_commission = fields.Float(string='Stove Sales Commission')
    additional_details = fields.Text(string='Additional Details')
    invoice_ids = fields.Many2many("account.invoice", string='Invoices', compute="_get_invoiced", readonly=True, copy=False)
    crm_invoice_count = fields.Integer(string='# of Invoices', compute='_get_invoiced', readonly=True)
    invoice_flag = fields.Boolean('Flag')
    invoice_id = fields.Many2one("account.invoice", string='Invoice')
    amount_paid = fields.Float(string='Amount Paid', compute='_get_amount_paid')
    amount_pending = fields.Float(string='Amount Pending', compute='_get_amount_paid')

    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        res = super(CrmLead, self)._onchange_stage_id()
        values = self._onchange_stage_id_values(self.stage_id.id)
        if self._origin.invoice_ids:
            if self._origin.invoice_ids.state == 'paid':
                self.update(values)
            else:
                raise UserError(_('You can not move any stage until you pay invoice.'))
        return res

    @api.multi
    def action_crm_from_view_invoice(self):
        invoices = self.mapped('invoice_ids')
        action = self.env.ref('account.action_invoice_tree1').read()[0]
        if len(invoices) > 1:
            action['domain'] = [('id', 'in', invoices.ids)]
        elif len(invoices) == 1:
            action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
            action['res_id'] = invoices.ids[0]
        else:
            action = {'type': 'ir.actions.act_window_close'}
        return action

    @api.multi
    def create_advance_invoices(self):
        account_invoic_line = self.env['account.invoice.line']
        product_product = self.env['reservation.product']
        product_product_list = product_product.search([])
        if not self.invoice_ids:
            if len(product_product_list) > 0:
                self.invoice_flag = True
                product_id = product_product_list[0].product_id.id
                name = product_product_list[0].product_id.name
                journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
                account_id = self.partner_id.property_account_receivable_id.id
                account = self.env['account.invoice'].create({
                    'partner_id': self.partner_id.id,
                    'date_invoice': datetime.date.today(),
                    'journal_id': journal_id,
                    'type': 'out_invoice',
                    'account_id': account_id,
                    'crm_lead_id': self.id,
                    })
                account_invoic_line.create({
                    'name': name,
                    'product_id': product_id,
                    'invoice_id': account.id,
                    'account_id': account_id,
                    'price_unit': self.down_payment_amount,
                    })
                self.invoice_id = account.id
                return {
                   'name': _('account.invoice_form'),
                   'view_type': 'form',
                   'view_mode': 'form',
                   'res_model': 'account.invoice',
                   'view_id': False,
                   'res_id': account.id,
                   'type': 'ir.actions.act_window',
                   }
            else:
                raise UserError(_('Please select one product which invoice is generated.'))
        else:
            raise UserError(_('Invoice already created for this lead.'))


class ReservationProduct(models.Model):
    _name = "reservation.product"
    _description = 'Reservation Product'
    _inherit = ['mail.thread']
    _rec_name = 'product_id'

    product_id = fields.Many2one("product.product", string='Product', required=True)

    @api.model
    def create(self, vals):
        product_list = self.env['reservation.product'].search([])
        if len(product_list) >= 1:
            raise UserError(_('You can not create reservation product more than 1.'))
        res = super(ReservationProduct, self).create(vals)
        return res

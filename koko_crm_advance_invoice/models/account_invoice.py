from odoo import api, fields, models, _


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    crm_lead_id = fields.Many2one('crm.lead', string='CRM')

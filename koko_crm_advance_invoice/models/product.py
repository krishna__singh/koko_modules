from odoo import api, fields, models


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    product_advance_invoice = fields.Boolean(string='Product for advance invoice')

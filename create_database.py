import erppeek
import xmlrpclib
import psycopg2

# DATABASE = raw_input("Enter name for Database? ")
# SERVER = raw_input("Enter URL with Port like - 'http://localhost:8029' ")

DATABASE = 'DemoDatabase'
SERVER = 'http://localhost:8069'
ADMIN_PASSWORD = 'admin@koko'

flag = True #False
client = erppeek.Client(server=SERVER)

if not DATABASE in client.db.list():
    print("The database does not exist yet, creating one!")
    client.create_database(ADMIN_PASSWORD, DATABASE)
    flag = True
    client = erppeek.Client(SERVER, DATABASE, 'admin', 'admin')

    # modules = client.modules('sale', installed=False)
    # if 'sale' in modules['uninstalled']:
    #     client.install('sale')
    #     print('The module sale has been installed!')
else:
    print("The database " + DATABASE + " already exists.")

if flag == True:
    sock_common = xmlrpclib.ServerProxy ('http://localhost:8069/xmlrpc/common')
    uid = sock_common.login(DATABASE, 'admin', 'admin')
    sock = xmlrpclib.ServerProxy('http://localhost:8069/xmlrpc/object')

    # uuid = sock.execute(DATABASE, uid, 'admin', 'ir.config_parameter', 'search', [('id', '=', 4)])
    # print'UUID found: ', uuid

    # Try to connect with new db
    try:
        conn = psycopg2.connect("dbname='DemoDatabase' user='odoo' host='localhost' password='odoo'")
        cur = conn.cursor()
        try:
            cur.execute("""SELECT * from ir_config_parameter""")
            rows = cur.fetchall()
            for row in rows:
                if row[1] == 'database.secret':
                    db_secret = row[2]
                elif row[1] == 'database.create_date':
                    create_date = row[2]
                elif row[1] == 'database.uuid':
                    uuid = row[2]
                elif row[1] == 'database.uuid':
                    uuid = row[2]

            print"##########", db_secret, create_date, uuid
            # Try to connect with old db
            try:
                con = psycopg2.connect("dbname='koko_development' user='odoo' host='localhost' password='odoo'")
                cursr = con.cursor()

                # update_uuid = "UPDATE ir_config_parameter SET value=(%s) WHERE id = (%s)", (uuid,4,)
                # print"update_uuid", update_uuid

                cursr.execute("UPDATE ir_config_parameter SET value=(%s) WHERE id = (%s)", (uuid,4,));
                con.commit()

                cursr.execute("UPDATE ir_config_parameter SET value=(%s) WHERE id = (%s)", (db_secret,1,));
                con.commit()

                cursr.execute("UPDATE ir_config_parameter SET value=(%s) WHERE id = (%s)", (create_date,3,));
                con.commit()

                cursr.execute("UPDATE ir_config_parameter SET value=(%s) WHERE key = (%s)", (5,'auth_signup.template_user_id',));
                con.commit()

                cursr.execute("DELETE from ir_config_parameter WHERE key = (%s)", ('database.expiration_date',));
                con.commit()

                cursr.execute("DELETE from ir_config_parameter WHERE key = (%s)", ('database.expiration_reason',));
                con.commit()

                cursr.close()


            except:
                print "I am unable to connect to old database"

        except:
            print "I can't SELECT from Table"

    except:
        print "I am unable to connect to new database"

    try:
        if DATABASE in client.db.list():
            client.db.drop('admin@koko', DATABASE)        

    except:
        print"unable to delete databse"      

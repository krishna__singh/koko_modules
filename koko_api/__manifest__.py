# -*- coding: utf-8 -*-

{
    'name': 'KOKO Networks Web Services',
    'version': '1.0',
    'category': 'CRM',
    'author': 'Prolitus Technologies Pvt. Ltd.',
    'sequence': 50,
    'summary': 'KOKO Networks Web Services',
    'depends': ['koko_crm_advance_invoice', 'koko_crm_extension'],
    'website': 'www.prolitus.com',
    'description': """
         Module provide the response of Web services.
    """,
    'data': [],
    'qweb': [],
    'application': True,
    'installable': True,
    'auto_install': False,
}

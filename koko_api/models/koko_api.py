# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import ast
import json
import logging
from datetime import datetime
from datetime import timedelta
from werkzeug._internal import _log
_logger = logging.getLogger(__name__)


class KokoApi(models.TransientModel):
    _name = 'koko.api'

    @api.model
    def done_inspection(self, done_inspection_datas=[]):

        done_inspection_datas = ast.literal_eval(done_inspection_datas)
        return_json = {'status': 0}
        inspection_list = []
        for done_inspection in done_inspection_datas:
            if 'inspection_id' in done_inspection:
                try:
                    insp = self.env['inspection'].search([('id', '=', done_inspection['inspection_id'])])
                    pipeline = self.env['crm.lead'].search([('id', '=', insp.lead_id.id)])
                    if insp:
                        for section_inspection in done_inspection['sections']:
                            section_id = section_inspection['section_id']
                            for section_task in section_inspection['tasks']:
                                attachment = self.env['ir.attachment']
                                task_id = section_task['task_id']
                                for inspection_task in insp.inspection_task_ids:
                                    if inspection_task.section_id.id == section_id and inspection_task.inspection_config_id.id == task_id:
                                        att_list = []
                                        for image in section_task['images']:
                                            filename = image['file_name'] + '.' + image['file_type']
                                            datas = image['file_data']
                                            att = attachment.create({
                                                'name': filename,
                                                'datas': datas,
                                                'datas_fname': filename,
                                                })
                                            att_list.append(att.id)
                                        if section_task['type'] == 'float':
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': section_task['values'],
                                                'float_value': section_task['values'],
                                                'image_ids': [(6, 0, att_list)]
                                                })
                                        elif section_task['type'] == 'integer':
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': section_task['values'],
                                                'integer_value': section_task['values'],
                                                'image_ids': [(6, 0, att_list)]
                                                })
                                        elif section_task['type'] == 'selection':
                                            value_id = self.env['inspection.tasks.value'].search([('id', '=', section_task['values'][0])])
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': value_id.name,
                                                'selection_id': section_task['values'][0],
                                                'image_ids': [(6, 0, att_list)]
                                                })
                                        elif section_task['type'] == 'many2many':
                                            str_many = ''
                                            for many in section_task['values']:
                                                value_id = self.env['inspection.tasks.value'].search([('id', '=', many)])
                                                str_many += value_id.name + ' '
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': str_many,
                                                'many2many_value': [(6, 0, section_task['values'])],
                                                'image_ids': [(6, 0, att_list)]
                                                })
                                        elif section_task['type'] == 'char':
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': section_task['values'],
                                                'char_vale': section_task['values'],
                                                'image_ids': [(6, 0, att_list)]
                                                })
                                        elif section_task['type'] == 'checkbox':
                                            values = 'Fail'
                                            check_values = False
                                            if section_task['values'] == 'true':
                                                values = 'Pass'
                                                check_values = True
                                            elif section_task['values'] == 'false':
                                                values = 'Fail'
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': values,
                                                'checkbox_value': check_values,
                                                'image_ids': [(6, 0, att_list)]
                                                })
                                        elif section_task['type'] == 'text':
                                            inspection_task.write({
                                                'comments': section_task['comments'],
                                                'value': section_task['values'],
                                                'checkbox_value': section_task['values'],
                                                'image_ids': [(6, 0, att_list)]
                                                })
                        insp.write({
                                'state': 'done',
                                'summary_of_inspection': done_inspection['summary_of_inspection'],
                                'lattitude': done_inspection['lattitude'],
                                'longitude': done_inspection['longitude'],
                                'shop_name': done_inspection['shop_name'],
                                'nearest_landmark': done_inspection['nearest_landmark'],
                                'pos_system': done_inspection['pos_system'],
                                'street': done_inspection['street'],
                                'street2': done_inspection['street2'],
                                'city': done_inspection['city'],
                                'zip': done_inspection['zip'],
                                'done_by_app': True,
                                'location_accuracy': done_inspection['location_accuracy']
                                })
                        if pipeline:
                            pipeline.write({
                                    'location_accuracy': done_inspection['location_accuracy']
                                    })
                        inspection_list.append(insp.id)
                    else:
                        return_json['msg'] = 'Given Inspection is wrong.'
                        return_json = json.dumps(return_json)
                except Exception, e:
                    try:
                        return_json['msg'] = e.name
                        return_json = json.dumps(return_json)
                    except Exception, e:
                        return_json['msg'] = str(e)
                        return_json = json.dumps(return_json)
            else:
                return_json['msg'] = 'You are requesting with wrong parameters.'
                return_json = json.dumps(return_json)
        if inspection_list:
            return_json['status'] = 1
            return_json['ids'] = inspection_list
            return_json = json.dumps(return_json)
            _logger.info("==if return_json====%s", return_json)
            return return_json
        else:
            _logger.info("==if return_json====%s", return_json)
            return return_json
#     @api.model
#     def cancel_inspection(self, params={}):
#         return_json = {'status': 0}
#         params = ast.literal_eval(params)
#         if 'inspection_id' in params:
#             try:
#                 insp = self.env['inspection'].search([('id', '=', params['inspection_id'])])
#                 if insp:
#                     insp.write({'state': 'cancel'})
#                     return_json['status'] = 1
#                     return_json = json.dumps(return_json)
#                     return return_json
#                 else:
#                     return_json['msg'] = 'Given Inspection is wrong.'
#                     return_json = json.dumps(return_json)
#                     return return_json
#             except Exception, e:
#                 try:
#                     return_json['msg'] = e.name
#                     return_json = json.dumps(return_json)
#                     return return_json
#                 except Exception, e:
#                     return_json['msg'] = str(e)
#                     return_json = json.dumps(return_json)
#                     return return_json
#         else:
#             return_json['msg'] = 'You are requesting with wrong parameters.'
#             return_json = json.dumps(return_json)
#             return return_json

    @api.model
    def reschedule_inspection(self, params_data=[]):
        params_data = ast.literal_eval(params_data)
        return_json = {'status': 0}
        reschedule_inspection_done_list = []
        for params in params_data:
            if 'inspection_id' in params and 'rescheduled_date' in params and 'reschedule_reason_id' in params:
                try:
                    insp = self.env['inspection'].search([('id', '=', params['inspection_id'])])
                    if insp:
                        initial_inspection_date = insp.date_of_inspection
                        rescheduled_datetime = params['rescheduled_date']
                        insp.write({
                                    'state': 'rescheduled',
                                    'date_of_inspection': rescheduled_datetime
                                    })
                        reschedule_reason_id = self.env['reschedule.reason.conf'].search([('id', '=', params['reschedule_reason_id'])])
                        rescheduled_note = ''
                        if reschedule_reason_id and reschedule_reason_id.is_comment:
                            rescheduled_note = params['rescheduled_note']
                        self.env['reschedule.details'].create({
                                                               'inspection_id': insp.id,
                                                               'rescheduled_date': datetime.now(),
                                                               'user_id': insp.user_id.id,
                                                               'new_inspection_date': rescheduled_datetime,
                                                               'initial_inspection_date': initial_inspection_date,
                                                               'reschedule_reason_id': reschedule_reason_id.id,
                                                               'reason_of_reschedule': rescheduled_note
                                                               })
                        reschedule_inspection_done_list.append(insp.id)
                    else:
                        return_json['msg'] = 'Given Inspection is wrong.'
                        return_json = json.dumps(return_json)
                except Exception, e:
                    try:
                        return_json['msg'] = e.name
                        return_json = json.dumps(return_json)
                    except Exception, e:
                        return_json['msg'] = str(e)
                        return_json = json.dumps(return_json)
            else:
                return_json['msg'] = 'You are requesting with wrong parameters.'
                return_json = json.dumps(return_json)
        if reschedule_inspection_done_list:
            return_json['status'] = 1
            return_json['ids'] = reschedule_inspection_done_list
            return_json = json.dumps(return_json)
            return return_json
        else:
            return return_json
#     def get_vendors(self):
#         vendor_list = []
#         for v in self.env['res.partner'].search([('supplier', '=', True), ('mobile_sync', '=', True)]):
#             vendor_list.append({'id': v.id, 'name': v.display_name})
#         return vendor_list

#     def get_inspection_reports(self, insp):
#         inspection_reports = []
#         for inspection_suggestion in insp.inspection_suggestion_ids:
#             inspection_reports_dic = {}
#             if inspection_suggestion.other:
#                 inspection_reports_dic['suggestion'] = inspection_suggestion.suggestion
#                 inspection_reports_dic['modification_suggestion_id'] = 0
#             else:
#                 inspection_reports_dic['suggestion'] = inspection_suggestion.modification_suggestion_id.name
#                 inspection_reports_dic['modification_suggestion_id'] = inspection_suggestion.modification_suggestion_id.id
#             vendor_list = []
#             for vendor in inspection_suggestion.partner_ids:
#                 vendor_dic = {}
#                 vendor_dic['id'] = vendor.id
#                 vendor_dic['name'] = vendor.name
#                 vendor_list.append(vendor_dic)
#             inspection_reports_dic['prefered_vendors'] = vendor_list
#             inspection_reports.append(inspection_reports_dic)
#         return inspection_reports

    def get_default_inspection_section_tasks(self):
        inspection_section_tasks = []
        inspection_section_tasks_list = self.env['inspection.section'].search([])
        for inspection_section_task in inspection_section_tasks_list:
            inspection_section_task_dic = {}
            inspection_section_task_dic['sequence'] = inspection_section_task.sequence
            inspection_section_task_dic['section_id'] = inspection_section_task.id
            inspection_section_task_dic['section_name'] = inspection_section_task.name
            inspection_task_list = []
            for inspection_task in inspection_section_task.inspection_tasks_conf_ids:
                inspection_task_dic = {}
                inspection_task_dic['id'] = inspection_task.id
                inspection_task_dic['name'] = inspection_task.name
                inspection_task_dic['inspection_type'] = inspection_task.inspection_type
                inspection_task_dic['is_image'] = inspection_task.is_image
                inspection_task_dic['task_info'] = inspection_task.task_info
                inspection_task_value = []
                for val in inspection_task.value_ids:
                    inspection_value_dic = {}
                    inspection_value_dic['name'] = val.name
                    inspection_value_dic['id'] = val.id
                    inspection_task_value.append(inspection_value_dic)
                inspection_task_dic['inspection_task_value'] = inspection_task_value
                inspection_task_list.append(inspection_task_dic)
            inspection_section_task_dic['task_list'] = inspection_task_list
            inspection_section_tasks.append(inspection_section_task_dic)
        return inspection_section_tasks

    def get_default_reschedule_reasons(self):
        reschedule_reasons = []
        reschedule_reasons_list = self.env['reschedule.reason.conf'].search([])
        for reschedule_reason in reschedule_reasons_list:
            reschedule_reason_dic = {}
            reschedule_reason_dic['id'] = reschedule_reason.id
            reschedule_reason_dic['name'] = reschedule_reason.name
            reschedule_reason_dic['is_comment'] = reschedule_reason.is_comment
            reschedule_reasons.append(reschedule_reason_dic)
        return reschedule_reasons

#     def get_default_modification_suggestions(self):
#         modification_suggestions = []
#         modification_suggestions_list = self.env['modification.suggestions'].search([])
#         for modification_suggestion in modification_suggestions_list:
#             modification_suggestions_dic = {}
#             modification_suggestions_dic['id'] = modification_suggestion.id
#             modification_suggestions_dic['name'] = modification_suggestion.name
#             modification_suggestions.append(modification_suggestions_dic)
#         return modification_suggestions

#     def get_preferred_vendors(self, insp):
#         vendors = []
#         for v in insp.preferred_vendors:
#             vendors.append({'id': v.id, 'name': v.name})
#         return vendors

#     def get_selection_values(self, t):
#         selection_values = []
#         for val in t.many2many_value:
#             selection_values.append(val.name)
#         return selection_values

#     def get_images(self, t):
#         image_datas = []
#         return image_datas

#     def inspection_section_tasks(self, insp):
#         tasks = []
#         for t in insp.inspection_task_ids:
#             tasks.append({
#                           'id': t.id,
#                           'inspection_type': t.inspection_type,
#                           'name': t.name,
#                           'char_value': t.char_vale,
#                           'float_value': t.float_value,
#                           'integer_value': t.integer_value,
#                           'selection_value': t.selection_id.name,
#                           'selection_values': self.get_selection_values(t),
#                           'checkbox_value': t.checkbox_value,
#                           'value': t.value,
#                           'is_image': t.is_image,
#                           'image_datas': self.get_images(t),
#                           })
#         return tasks

    def get_inspections(self):
        _logger.warning("method called: get_inspections")
        inspections = []
        for insp in self.env['inspection'].search([('user_id', '=', self._uid), ('state', 'in', ['scheduled', 'rescheduled'])]):
            d_date_time = datetime.strptime(insp.date_of_inspection, "%Y-%m-%d %H:%M:%S")
            change_date = d_date_time + timedelta(minutes=180)
            final_date = str(change_date)
            vals = {
                    'id': insp.id,
                    'ref': insp.ref,
                    'date_of_inspection': final_date,
                    'agent_name': insp.agent_name,
                    'street': insp.street,
                    'street2': insp.street2,
                    'zip': insp.zip,
                    'city': insp.city,
                    'add_state': insp.state_id.name,
                    'country': insp.country_id.name,
                    'contact_person': insp.agent_name,
                    'shop_name': insp.shop_name,
                    'manager_name': insp.manager_name,
                    'manager_contact': insp.manager_contact,
                    'pos_system': insp.pos_system,
                    'lattitude': insp.lattitude,
                    'longitude': insp.longitude,
                    'tdr_id': insp.tdr_rm_id.id,
                    'tdr_name': insp.tdr_rm_id.name,
                    'tdr_contact': insp.tdr_rm_id.phone,
                    'max_image': insp.max_image,
                    'nearest_landmark': insp.nearest_landmark,
                    'agent_contact': insp.agent_contact,
                    'location_accuracy': insp.location_accuracy
                    }
            if insp.state == 'scheduled':
                vals['state'] = 'Scheduled'
                inspections.append(vals)
            elif insp.state == 'rescheduled':
                vals['state'] = 'Rescheduled'
                inspections.append(vals)
        return inspections

    @api.model
    def get_default_data(self, params={}):
        try:
            return_json = {'status': 0}
            return_json['default_values'] = self.get_inspections()
            return_json['default_inspection_section_tasks'] = self.get_default_inspection_section_tasks()
            return_json['default_reschedule_reasons'] = self.get_default_reschedule_reasons()
            return_json['status'] = 1
            return json.dumps(return_json)
        except Exception, e:
            try:
                return_json['msg'] = e.name
                return_json = json.dumps(return_json)
                return return_json
            except Exception, e:
                return_json['msg'] = str(e)
                return_json = json.dumps(return_json)
                return return_json
